import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { tap } from 'rxjs/operators';
import { MatchCalendar } from '../match-calendar';
import { Current } from '../current';
import { Cricket } from '../cricket';
import { Favourite } from '../favourite';
import { Recommend } from '../recommend';

@Injectable()
export class NotesService {
  matchCalendars: Array<MatchCalendar>;
  matchCalendarsSubject: BehaviorSubject<Array<MatchCalendar>>;

  matchs: Array<Current>;
  matchsSubject: BehaviorSubject<Array<Current>>;

  crickets: Array<Cricket>;
  cricketsSubject: BehaviorSubject<Array<Cricket>>;

  favourites: Array<Favourite>;
  favouritsSubject: BehaviorSubject<Array<Favourite>>;

  recommends: Array<Recommend>;
  recommendsSubject: BehaviorSubject<Array<Recommend>>;

  constructor(private httpClient: HttpClient,
    private authService: AuthenticationService) {
    this.matchCalendars = [];
    this.matchCalendarsSubject = new BehaviorSubject([]);
    this.matchs = [];
    this.matchsSubject = new BehaviorSubject([]);
    this.crickets = [];
    this.cricketsSubject = new BehaviorSubject([]);
    this.favourites = [];
    this.favouritsSubject = new BehaviorSubject([]);
    this.recommends = [];
    this.recommendsSubject = new BehaviorSubject([]);
  }

  fetchMatchCalendarsFromServer() {
    return this.httpClient.get<Array<MatchCalendar>>('http://localhost/api-match/getMatchCalendar', {
      headers: new HttpHeaders().set('Authorization', `Bearer ${this.authService.getBearerToken()}`)
    }).subscribe(response => {
      this.matchCalendars = response;
      this.matchCalendarsSubject.next(this.matchCalendars);
    }, error => {
      this.matchCalendars = [];
      this.matchCalendarsSubject.next(this.matchCalendars);
    });
  }

  getMatchCalendars(): BehaviorSubject<Array<MatchCalendar>> {
    return this.matchCalendarsSubject;
  }

  fetchMatchsFromServer() {
    return this.httpClient.get<Array<Current>>('http://localhost/api-match/getMatches', {
      headers: new HttpHeaders().set('Authorization', `Bearer ${this.authService.getBearerToken()}`)
    }).subscribe(response => {
      this.matchs = response;
      this.matchsSubject.next(this.matchs);
    }, error => {
      this.matchs = [];
      this.matchsSubject.next(this.matchs);
    });
  }

  getMatchs(): BehaviorSubject<Array<Current>> {
    return this.matchsSubject;
  }

  fetchCricketsFromServer() {
    return this.httpClient.get<Array<Cricket>>('http://localhost/api-match/getCricket', {
      headers: new HttpHeaders().set('Authorization', `Bearer ${this.authService.getBearerToken()}`)
    }).subscribe(response => {
      this.crickets = response;
      this.cricketsSubject.next(this.crickets);
    }, error => {
      this.crickets = [];
      this.cricketsSubject.next(this.crickets);
    });
  }

  getCrickets(): BehaviorSubject<Array<Cricket>> {
    return this.cricketsSubject;
  }

  fetchFavouritesFromServer() {
    const userId = this.authService.getUserId();
    return this.httpClient.get<Array<Favourite>>(`http://localhost/api-favourite/getFavourite?userId=${userId}`, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${this.authService.getBearerToken()}`)
    }).subscribe(response => {
      this.favourites = response;
      this.favouritsSubject.next(this.favourites);
    }, error => {
      this.favourites = [];
      this.favouritsSubject.next(this.favourites);
    });
  }

  getFavourites(): BehaviorSubject<Array<Favourite>> {
    return this.favouritsSubject;
  }

  addFavourite(favourire: Favourite): Observable<Favourite> {
    return this.httpClient.post<Favourite>('http://localhost/api-favourite/addFavourite', favourire, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${this.authService.getBearerToken()}`)
    }).pipe(
      tap(
        response => {
          this.favourites.push(response);
          this.favouritsSubject.next(this.favourites);
        }));
  }

  fetchRecommendsFromServer() {
    const userId = this.authService.getUserId();
    return this.httpClient.get<Array<Recommend>>(`http://localhost/api-favourite/getRecommends`, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${this.authService.getBearerToken()}`)
    }).subscribe(response => {
      this.recommends = response;
      this.recommendsSubject.next(this.recommends);
    }, error => {
      this.recommends = [];
      this.recommendsSubject.next(this.recommends);
    });
  }

  getRecommends(): BehaviorSubject<Array<Recommend>> {
    return this.recommendsSubject;
  }

  /* addNote(note: Note): Observable<Note> {
    return this.httpClient.post<Note>('http://localhost:3000/api/v1/notes', note, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${this.authService.getBearerToken()}`)
    }).pipe(
      tap(
        response => {
          this.notes.push(response);
          this.notesSubject.next(this.notes);
        }));
  }

  editNote(note: Note): Observable<Note> {
    return this.httpClient.put<Note>(`http://localhost:3000/api/v1/notes/${note.id}`, note, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${this.authService.getBearerToken()}`)
    }).pipe(
      tap(
        editedNote => {
          const foundNote = this.notes.find(cnote => cnote.id === editedNote.id);
          Object.assign(foundNote, editedNote);
          this.notesSubject.next(this.notes);
        }));
  }

  getNoteById(noteId: Number): Note {
    return this.notes.find(note => note.id === noteId);
  } */
}
