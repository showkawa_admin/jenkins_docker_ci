import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Favourite } from '../favourite';
import { NotesService } from '../services/notes.service';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {


  submitMessage: string;

  constructor(public dialogRef: MatDialogRef<NoteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Favourite,
    private noteServie: NotesService) { }

  ngOnInit() {
    this.submitMessage = '';
  }

  add() {
    this.noteServie.addFavourite(this.data['favourite']).subscribe(res => {
      console.log('1234567');
    }, error => {
      this.submitMessage = (error.error != null) ? error.error.message : error.message;
    });
    this.dialogRef.close();
  }
}
