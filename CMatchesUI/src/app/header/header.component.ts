import { Component } from '@angular/core';
import { RouterService } from '../services/router.service';
import { queueScheduler } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  isNoteView = true;

  constructor(private routerService: RouterService) {
  }

  openMenu() {
    console.log(document.querySelector('.cimacth-nav-lg'));
    document.querySelector('.cimacth-nav-lg')['style']['display'] = 'block';

    this.isNoteView = false;
  }

  closeMenu() {
    document.querySelector('.cimacth-nav-lg')['style']['display'] = 'none';
    this.isNoteView = true;
  }
}
