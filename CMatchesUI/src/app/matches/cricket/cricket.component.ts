import { Component, OnInit } from '@angular/core';
import { Cricket } from '../../cricket';
import { NotesService } from '../../services/notes.service';

@Component({
  selector: 'app-cricket',
  templateUrl: './cricket.component.html',
  styleUrls: ['./cricket.component.css']
})
export class CricketComponent implements OnInit {

  matchs: Array<Cricket>;
  errorMessage: String;
  displayedColumns: string[] = ['uniqueId', 'title', 'description'];
  dataSource = Array<Cricket>();

  constructor(private noteService: NotesService) {
    this.matchs = [];
    this.dataSource = [];
   }

  ngOnInit() {
    this.noteService.getCrickets().subscribe(res => {
      this.dataSource = res;
    },
      error => {
        this.errorMessage = error.message;
      });
  }

}
