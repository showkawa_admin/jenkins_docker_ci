import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotesService } from '../services/notes.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {

  constructor(private notesService: NotesService) {
    this.notesService.fetchMatchsFromServer();
    this.notesService.fetchCricketsFromServer();
  }

  ngOnInit() {

  }

}
