import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Current } from '../../current';
import { NotesService } from '../../services/notes.service';
import { MatDialog } from '@angular/material';
import { NoteComponent } from '../../note/note.component';
import { Favourite } from '../../favourite';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-current',
  templateUrl: './current.component.html',
  styleUrls: ['./current.component.css']
})
export class CurrentComponent implements OnInit {

  matchs: Array<Current>;
  errorMessage: String;
  displayedColumns: string[] = ['uniqueId', 'team1', 'team2', 'matchStarted', 'date', 'action'];
  dataSource = Array<Current>();
  favourite: Favourite;

  constructor(private noteService: NotesService, public dialog: MatDialog, private authService: AuthenticationService) {
    this.matchs = [];
    this.dataSource = [];
   }

  ngOnInit() {
    this.noteService.getMatchs().subscribe(res => {
      this.dataSource = res;
    },
      error => {
        this.errorMessage = error.message;
      });
  }

  addFavourite(current: Current): void {
    console.log(current);
     this.favourite = new Favourite();
     this.favourite.userId = this.authService.getUserId();
     console.log(this.authService.getUserId());
     this.favourite.uniqueId = current.uniqueId;
     this.favourite.matches = current.team1 + ' VS ' + current.team2;
    const dialogRef = this.dialog.open(NoteComponent, {
      width: '250px',
      data: {favourite: this.favourite}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed > ' + result);
    });
  }

}
