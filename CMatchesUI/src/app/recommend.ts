export class Recommend {
    uniqueId: string;
    weight: number;
    matches: string;

    constructor() {
      this.uniqueId = '';
      this.weight = 1;
      this.matches = '';
    }
}
