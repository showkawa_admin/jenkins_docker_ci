export class Current {
    uniqueId: string;
    team1: string;
    team2: string;
    date: string;
    matchStarted: boolean;

    constructor() {
      this.uniqueId = '';
      this.team1 = '';
      this.team2 = '';
      this.date = '';
      this.matchStarted = false;
    }
}
