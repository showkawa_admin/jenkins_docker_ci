export class MatchCalendar {
    uniqueId: string;
    name: string;
    date: string;

    constructor() {
      this.uniqueId = '';
      this.name = '';
      this.date = '';
    }
}