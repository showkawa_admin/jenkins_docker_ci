import { Component, OnInit } from '@angular/core';
import { MatchCalendar } from '../match-calendar';
import { NotesService } from '../services/notes.service';

@Component({
  selector: 'app-cricket-calendar',
  templateUrl: './cricket-calendar.component.html',
  styleUrls: ['./cricket-calendar.component.css']
})
export class CricketCalendarComponent implements OnInit {

  matchCalendars: Array<MatchCalendar>;
  errorMessage: String;
  displayedColumns: string[] = ['uniqueId', 'name', 'date'];
  dataSource = Array<MatchCalendar>();

  constructor(private noteService: NotesService) {
    this.matchCalendars = [];
    this.dataSource = [];
   }

  ngOnInit() {
    this.noteService.getMatchCalendars().subscribe(res => {
      this.dataSource = res;
    },
      error => {
        this.errorMessage = error.message;
      });
  }


}
