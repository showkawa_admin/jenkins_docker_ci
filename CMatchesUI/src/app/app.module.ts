import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatMenu, MatMenuModule, MatTableModule } from '@angular/material';
import { HeaderComponent } from './header/header.component';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AuthenticationService } from './services/authentication.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterService } from './services/router.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCardModule } from '@angular/material/card';
import { NotesService } from './services/notes.service';
import { CanActivateRouteGuard } from './can-activate-route.guard';
import { NoteComponent } from './note/note.component';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { CricketCalendarComponent } from './cricket-calendar/cricket-calendar.component';
import { MatchesComponent } from './matches/matches.component';
import { FavouriteComponent } from './favourite/favourite.component';
import { RecommendedComponent } from './recommended/recommended.component';
import { CurrentComponent } from './matches/current/current.component';
import { CricketComponent } from './matches/cricket/cricket.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'dashboard', component: DashboardComponent, canActivate: [CanActivateRouteGuard],
    children: [
      { path: 'view/cricket-calendar', component: CricketCalendarComponent },
      { path: 'view/matches', component: MatchesComponent, children: [
        { path: 'current', component: CurrentComponent },
        { path: 'cricket', component: CricketComponent},
        { path: '', component: CurrentComponent}
      ]
     },
      { path: 'view/favourite', component: FavouriteComponent },
      { path: 'view/recommended', component: RecommendedComponent },
      { path: '', redirectTo: 'view/cricket-calendar', pathMatch: 'full' }
    ]
  },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    LoginComponent,
    NoteComponent,
    CricketCalendarComponent,
    MatchesComponent,
    FavouriteComponent,
    RecommendedComponent,
    CurrentComponent,
    CricketComponent
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCheckboxModule,
    BrowserModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatExpansionModule,
    MatCardModule,
    MatDialogModule,
    MatSelectModule,
    MatMenuModule,
    MatTableModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthenticationService,
    RouterService,
    NotesService,
    CanActivateRouteGuard,
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  exports: [RouterModule],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent],
  entryComponents: [NoteComponent]
})

export class AppModule { }
