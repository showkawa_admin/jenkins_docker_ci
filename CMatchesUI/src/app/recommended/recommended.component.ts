import { Component, OnInit } from '@angular/core';
import { Recommend } from '../recommend';
import { NotesService } from '../services/notes.service';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-recommended',
  templateUrl: './recommended.component.html',
  styleUrls: ['./recommended.component.css']
})
export class RecommendedComponent implements OnInit {
  matchs: Array<Recommend>;
  errorMessage: String;
  displayedColumns: string[] = ['uniqueId', 'matches', 'weight'];
  dataSource = Array<Recommend>();

  constructor(private noteService: NotesService,  private authService: AuthenticationService) {
    this.matchs = [];
    this.dataSource = [];
    this.noteService.fetchRecommendsFromServer();
   }

  ngOnInit() {
    this.noteService.getRecommends().subscribe(res => {
      this.dataSource = res;
    },
      error => {
        this.errorMessage = error.message;
      });
  }

}
