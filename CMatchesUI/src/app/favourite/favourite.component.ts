import { Component, OnInit } from '@angular/core';
import { Favourite } from '../favourite';
import { AuthenticationService } from '../services/authentication.service';
import { NotesService } from '../services/notes.service';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {

  matchs: Array<Favourite>;
  errorMessage: String;
  displayedColumns: string[] = ['uniqueId', 'matches'];
  dataSource = Array<Favourite>();
  favourite: Favourite;

  constructor(private noteService: NotesService,  private authService: AuthenticationService) {
    this.matchs = [];
    this.dataSource = [];
    this.noteService.fetchFavouritesFromServer();
   }

  ngOnInit() {
    this.noteService.getFavourites().subscribe(res => {
      this.dataSource = res;
    },
      error => {
        this.errorMessage = error.message;
      });
  }


}
