export class Favourite {
    uniqueId: string;
    userId: string;
    matches: string;

    constructor() {
      this.uniqueId = '';
      this.userId = '';
      this.matches = '';
    }
}
