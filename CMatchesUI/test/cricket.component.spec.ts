import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CricketComponent } from '../src/app/matches/cricket/cricket.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NotesService } from '../src/app/services/notes.service';
import { AuthenticationService } from '../src/app/services/authentication.service';
import { MatTableModule } from '@angular/material';

describe('CricketComponent', () => {
  let component: CricketComponent;
  let fixture: ComponentFixture<CricketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CricketComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ HttpClientModule ,
        MatTableModule ],
      providers: [ NotesService, AuthenticationService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CricketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
