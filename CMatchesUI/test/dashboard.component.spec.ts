import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from '../src/app/dashboard/dashboard.component';
import { NotesService } from '../src/app/services/notes.service';
import { AuthenticationService } from '../src/app/services/authentication.service';
import {of} from 'rxjs';


const testConfig = {
  matchCalendars: [{
    uniqueId: 1186493,
    name: 'Maldives v Thailand at Kuala Lumpur, 6th Match',
    date: '29 June 2019'
    },
    {
      uniqueId: 1190608,
      name: 'Germany Women v Scotland Women at Cartagena, 5th Match',
      date: '29 June 2019'
    },
      {
        uniqueId: 1144518,
        name: 'Afghanistan v Pakistan at Leeds, 36th match',
        date: '29 June 2019'
    }]
};

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let notesService: any;
  let fetchMatchCalendarsFromServer: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardComponent ],
      imports: [ HttpClientModule ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
      NotesService,
      AuthenticationService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    notesService = TestBed.get(NotesService);
    fetchMatchCalendarsFromServer = spyOn(notesService, 'fetchMatchCalendarsFromServer').and.returnValue(of(testConfig.matchCalendars));
  });

  it('should create', () => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });

  it('fetchMatchCalendarsFromServer should be called whenever DashboardComponent is rendered', () => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    expect(notesService.fetchMatchCalendarsFromServer).toHaveBeenCalled();
  });
});
