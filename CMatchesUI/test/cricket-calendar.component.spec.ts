import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { CricketCalendarComponent } from '../src/app/cricket-calendar/cricket-calendar.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NotesService } from '../src/app/services/notes.service';
import { AuthenticationService } from '../src/app/services/authentication.service';
import { of } from 'rxjs';
import { MatTableModule } from '@angular/material';

const testConfig = {
  matchCalendars: [{
    uniqueId: 1186493,
    name: 'Maldives v Thailand at Kuala Lumpur, 6th Match',
    date: '29 June 2019'
    },
    {
      uniqueId: 1190608,
      name: 'Germany Women v Scotland Women at Cartagena, 5th Match',
      date: '29 June 2019'
    },
      {
        uniqueId: 1144518,
        name: 'Afghanistan v Pakistan at Leeds, 36th match',
        date: '29 June 2019'
    }]
};


describe('CricketCalendarComponent', () => {
  let component: CricketCalendarComponent;
  let fixture: ComponentFixture<CricketCalendarComponent>;
  let notesService: NotesService;
  let spyGetNotes: any;
  let response: Array<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CricketCalendarComponent],
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ HttpClientModule ,
        MatTableModule ],
      providers: [ NotesService, AuthenticationService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CricketCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    notesService = fixture.debugElement.injector.get(NotesService);
  });

  it('should handle get all cricketCalendars', fakeAsync(() => {
    response = testConfig.matchCalendars;
    spyGetNotes = spyOn(notesService, 'getMatchCalendars').and.returnValue(of(response));
    fixture.detectChanges();
    expect(testConfig.matchCalendars).toBe(response, `should get all cricketCalendars from back end`);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
