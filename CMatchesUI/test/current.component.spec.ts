import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { CurrentComponent } from '../src/app/matches/current/current.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NotesService } from '../src/app/services/notes.service';
import { AuthenticationService } from '../src/app/services/authentication.service';
import { MatTableModule, MatDialog, MatDialogModule } from '@angular/material';
import { of } from 'rxjs';

const testConfig = {
  currentMatches: [{
    uniqueId: 1144517,
    team1: 'South Africa',
    team2: 'Sri Lanka',
    date: '2019-06-28 09:30:00',
    matchStarted: true
    }]
};

describe('CurrentComponent', () => {
  let component: CurrentComponent;
  let fixture: ComponentFixture<CurrentComponent>;
  let notesService: NotesService;
  let spyGetNotes: any;
  let response: Array<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      imports: [ HttpClientModule ,
        MatTableModule, MatDialogModule ],
      providers: [ NotesService, AuthenticationService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    notesService = fixture.debugElement.injector.get(NotesService);
  });

  it('should handle get all current matches', fakeAsync(() => {
    response = testConfig.currentMatches;
    spyGetNotes = spyOn(notesService, 'getMatchs').and.returnValue(of(response));
    fixture.detectChanges();
    expect(testConfig.currentMatches).toBe(response, `should get all current matches from back end`);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
