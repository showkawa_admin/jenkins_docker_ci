package com.stackroute.userservice.repository;

import com.stackroute.userservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;




@Repository
public interface UserAutheticationRepository extends JpaRepository<User, String> {

    User findByUserIdAndUserPassword(String userId, String userPassword);
}
