package com.stackroute.matchrecommendationservice.domain;

import lombok.Data;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 12
 **/
@Data
public class Cricket {

    private String uniqueId;
    private String title;
    private String description;

    public Cricket() {
    }

    public Cricket(String uniqueId, String title, String description) {
        this.uniqueId = uniqueId;
        this.title = title;
        this.description = description;
    }
}
