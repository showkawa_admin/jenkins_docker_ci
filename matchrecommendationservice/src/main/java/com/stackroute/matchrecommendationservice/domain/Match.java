package com.stackroute.matchrecommendationservice.domain;

import lombok.Data;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 12
 **/
@Data
public class Match {

    private String uniqueId;
    private String team1;
    private String team2;
    private String date;
    private boolean matchStarted;

    public Match() {
    }

    public Match(String uniqueId, String team1, String team2, String date, boolean matchStarted) {
        this.uniqueId = uniqueId;
        this.team1 = team1;
        this.team2 = team2;
        this.date = date;
        this.matchStarted = matchStarted;
    }
}
