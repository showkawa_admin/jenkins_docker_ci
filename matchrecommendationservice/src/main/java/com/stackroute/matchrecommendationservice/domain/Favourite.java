package com.stackroute.matchrecommendationservice.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 17
 **/
@Data
@Entity
public class Favourite {
    @Id
    private int id;
    @NotNull
    private String userId;
    @NotNull
    private String uniqueId;
    @NotNull
    private String matches;
}
