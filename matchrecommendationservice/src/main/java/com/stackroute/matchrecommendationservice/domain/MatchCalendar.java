package com.stackroute.matchrecommendationservice.domain;

import lombok.Data;

import java.util.Date;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 09
 **/
@Data
public class MatchCalendar {
    private String uniqueId;
    private String name;
    private String date;

    public MatchCalendar() {
    }

    public MatchCalendar(String uniqueId, String name, String date) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.date = date;
    }
}
