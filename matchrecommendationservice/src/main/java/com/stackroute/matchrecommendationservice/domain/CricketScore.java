package com.stackroute.matchrecommendationservice.domain;

import lombok.Data;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 12
 **/
@Data
public class CricketScore {

    private String uniqueId;
    private String team1;
    private String team2;
    private boolean matchStarted;
    private String stat;
    private String score;
    private String description;

    public CricketScore() {
    }
}
