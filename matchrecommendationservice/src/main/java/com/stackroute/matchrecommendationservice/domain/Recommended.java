package com.stackroute.matchrecommendationservice.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-07-06 17
 **/
@Data
@Entity
public class Recommended {

    @Id
    private int id;
    @NotNull
    private String matches;
    @NotNull
    private String uniqueId;
    @NotNull
    private int weight;
}
