package com.stackroute.matchrecommendationservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.stackroute.matchrecommendationservice.domain.Favourite;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-07-06 18
 **/
@Component
@Slf4j
public class MatchMqService {

    @Autowired
    @Qualifier("recommendServiceL")
    private  RecommendService recommendService;

       @RabbitListener(queues = "CIMATCH")
       public void process(Message message, Channel channel) throws IOException {

           String str = new String(message.getBody());
           log.info("receive cimatch: " + str);
           ObjectMapper mapper = new ObjectMapper();
           Favourite favourite = mapper.readValue(str, Favourite.class);
           recommendService.save(favourite.getUniqueId(),favourite.getMatches());
           channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
       }

}
