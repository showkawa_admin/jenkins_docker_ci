package com.stackroute.matchrecommendationservice.service;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.stackroute.matchrecommendationservice.domain.Cricket;
import com.stackroute.matchrecommendationservice.domain.CricketScore;
import com.stackroute.matchrecommendationservice.domain.Match;
import com.stackroute.matchrecommendationservice.domain.MatchCalendar;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-29 17
 **/
@FeignClient(url = "${cric.apiUrl}",name="crciurl",fallback = FeignServiceCallback.class)
public interface FeignService {

    //http://cricapi.com/api/cricket?apikey=OD8EHhcjfCMgZrIkB1u0B29oYBD3
    @GetMapping("/cricket")
    public List<Cricket> getCricket(@RequestParam("apikey") String apikey);

    //https://cricapi.com/api/matches/?apikey=OD8EHhcjfCMgZrIkB1u0B29oYBD3
    @GetMapping("/getMatches")
    public List<Match> getMatches(@RequestParam("apikey")String apikey );

    //http://cricapi.com/api/cricketScore?unique_id=1144516&apikey=OD8EHhcjfCMgZrIkB1u0B29oYBD3
    @GetMapping("/getCricketScore")
    public CricketScore getCricketScore(@RequestParam("apikey")String apikey, @RequestParam("unique_id")String uniqueId );

    //http://cricapi.com/api/matchCalendar?apikey=OD8EHhcjfCMgZrIkB1u0B29oYBD3
    @GetMapping("/getMatchCalendar")
    public List<MatchCalendar> getMatchCalendar(@RequestParam("apikey")String apikey);

    }


