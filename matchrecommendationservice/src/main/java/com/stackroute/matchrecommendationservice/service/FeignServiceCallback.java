package com.stackroute.matchrecommendationservice.service;

import com.stackroute.matchrecommendationservice.domain.Cricket;
import com.stackroute.matchrecommendationservice.domain.CricketScore;
import com.stackroute.matchrecommendationservice.domain.Match;
import com.stackroute.matchrecommendationservice.domain.MatchCalendar;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 12
 **/
@Component
public class FeignServiceCallback implements FeignService{

    @Override
    public List<Cricket> getCricket(String apikey) {
        List<Cricket> list = Arrays.asList(
                new Cricket("1144520","England v India","England v India"),
                new Cricket("1167005","Derbyshire v Middlesex","Derbyshire v Middlesex"),
                new Cricket("1166941","Glamorgan v Worcestershire","Glamorgan v Worcestershire"),
                new Cricket("1167006","Kent v Warwickshire","Kent v Warwickshire"),
                new Cricket("1167007","Lancashire v Durham","Lancashire v Durham")

        );
        return list;
    }

    @Override
    public List<Match> getMatches(String apikey) {
        List<Match> matches = Arrays.asList(
                new Match("1144517",
                        "South Africa",
                        "Sri Lanka",
                        "2019-06-28 09:30:00",
                        true
                        ),
                new Match("1186492",
                        "Malaysia",
                        "Maldives",
                        "2019-06-28 02:00:00",
                        true
                        ),
                new Match("1190841",
                        "England Academy Women",
                        "Australia Women",
                        "2019-06-28 09:30:00",
                        true
                        ),
                new Match("1144518",
                        "Afghanistan",
                        "Pakistan",
                        "2019-06-29 09:30:00",
                        true
                        ),
                new Match("1144519",
                        "Australia",
                        "New Zealand",
                        "2019-06-29 12:30:00",
                        true
                        )

        );
        return matches;
    }

    @Override
    public CricketScore getCricketScore(String apikey,String uniqueId) {
        CricketScore  cricketScore = new CricketScore();
        cricketScore.setUniqueId(uniqueId);
        cricketScore.setStat("India won by 125 runs");
        cricketScore.setScore("India 268/7 v West Indies 143/10 *");
        cricketScore.setTeam1("India");
        cricketScore.setTeam2("West Indies");
        cricketScore.setDescription("India 268/7 v West Indies 143/10 *");
        cricketScore.setMatchStarted(true);
        return null;
    }

    @Override
    public List<MatchCalendar> getMatchCalendar(String apikey) {

        List<MatchCalendar> matchControllerList = Arrays.asList(
                new MatchCalendar("1186493",
                        "Maldives v Thailand at Kuala Lumpur, 6th Match",
                        "29 June 2019"),
                new MatchCalendar("1190608",
                        "Germany Women v Scotland Women at Cartagena, 5th Match",
                        "29 June 2019"),
                new MatchCalendar("1144518",
                        "Afghanistan v Pakistan at Leeds, 36th match",
                        "29 June 2019")
        );
        return matchControllerList;
    }
}
