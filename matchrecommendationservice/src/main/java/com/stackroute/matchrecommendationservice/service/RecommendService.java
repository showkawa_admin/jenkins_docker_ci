package com.stackroute.matchrecommendationservice.service;


import com.stackroute.matchrecommendationservice.domain.Recommended;
import com.stackroute.matchrecommendationservice.repository.RecommendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 17
 **/
@Service("recommendServiceL")
public class RecommendService {

    private AtomicInteger weight;

    @Autowired
    @Qualifier("recommendRepositoryL")
    private RecommendRepository recommendRepository;

    public RecommendService() {
        this.weight = new AtomicInteger();
    }

    public Recommended save(String uniqueId, String matches){
        Recommended rec = new Recommended();
        Recommended recommended = recommendRepository.findByUniqueIdOrderByWeightDesc(uniqueId);
        rec.setUniqueId(uniqueId);
        rec.setMatches(matches);
        if(recommended == null){
            rec.setWeight(1);
        }else{
            rec.setId(recommended.getId());
            weight.set(recommended.getWeight());
            weight.incrementAndGet();
            rec.setWeight(weight.get());
        }
        return recommendRepository.save(rec);
    }


}
