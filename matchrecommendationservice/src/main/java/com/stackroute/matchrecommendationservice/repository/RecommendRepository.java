package com.stackroute.matchrecommendationservice.repository;

import com.stackroute.matchrecommendationservice.domain.Recommended;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 17
 **/
@Repository("recommendRepositoryL")
public interface RecommendRepository extends JpaRepository<Recommended, String> {

    public Recommended findByUniqueIdOrderByWeightDesc(String uniqueId);


}
