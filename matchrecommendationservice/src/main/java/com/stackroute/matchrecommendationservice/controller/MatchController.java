package com.stackroute.matchrecommendationservice.controller;

import com.stackroute.matchrecommendationservice.domain.Cricket;
import com.stackroute.matchrecommendationservice.domain.CricketScore;
import com.stackroute.matchrecommendationservice.domain.Match;
import com.stackroute.matchrecommendationservice.domain.MatchCalendar;
import com.stackroute.matchrecommendationservice.service.FeignService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-29 17
 **/
@RestController
@Api("Match API Service")
public class MatchController {

    @Autowired
    private FeignService feignService;
    @Value("${cric.apiKey}")
    private String apikey;



    @GetMapping("/getCricket")
    @ApiOperation("Get the old matches")
    public ResponseEntity<List<Cricket>> getCricket() throws Exception {
        List<Cricket> cricket = feignService.getCricket(apikey);
        return new ResponseEntity<>(cricket, HttpStatus.OK);

    }

    @GetMapping("/getMatches")
    @ApiOperation("Get the current matches")
    public ResponseEntity<List<Match>> getMatches() throws Exception {
        List<Match> matches = feignService.getMatches(apikey);
        return new ResponseEntity<>(matches, HttpStatus.OK);
    }

    @GetMapping("/getCricketScore")
    @ApiOperation("Get the cricket score")
    @ApiImplicitParam(name = "uniqueId",value = "uniqueId",required = true,dataTypeClass = String.class)
    public ResponseEntity<CricketScore> getCricketScore(@RequestParam("unique_id") String uniqueId) throws Exception {
        CricketScore cricketScore = feignService.getCricketScore(apikey, uniqueId);
        return new ResponseEntity<>(cricketScore, HttpStatus.OK);
    }

    @GetMapping("/getMatchCalendar")
    @ApiOperation("Get the match calendar")
    public ResponseEntity<List<MatchCalendar>> getMatchCalendar() throws Exception {
        List<MatchCalendar> matchCalendar = feignService.getMatchCalendar(apikey);
        return new ResponseEntity<>(matchCalendar, HttpStatus.OK);

    }
}
