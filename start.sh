#!/bin/bash

echo "Stop and clean all container ..."
docker container rm $(docker container ls -a -q)  -f
docker-compose rm -v

echo "Clean all images with sba tag ..."
docker rmi $(docker images | grep sba-0.0.1 | tr -s ' ' | cut -d ' ' -f 3)

#echo "Package all maven project ..."
#mvn clean package -Dmaven.test.skip=true

#echo "Remove dist of Angular project and then build it again ..."
#rm -rf CMatchesUI/dist
#cd CMatchesUI
#npm i && ng build

#echo "docker-compose build and up ..."
#cd ..

docker-compose build
docker-compose up -d