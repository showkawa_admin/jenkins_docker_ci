package com.stackroute.favouriteservice.controller;

import com.stackroute.favouriteservice.domian.Recommended;
import com.stackroute.favouriteservice.service.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-07-06 17
 **/
@RestController
public class RecommendController {

    @Autowired
    private RecommendService recommendService;

    @GetMapping("/getRecommends")
    public ResponseEntity<List<Recommended>> getFavourite() throws Exception {
        List<Recommended> recommends = recommendService.getRecommends();
        return new ResponseEntity<>(recommends, HttpStatus.OK);
    }
}
