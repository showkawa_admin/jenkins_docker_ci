package com.stackroute.favouriteservice.controller;

import com.stackroute.favouriteservice.domian.Favourite;
import com.stackroute.favouriteservice.service.FavouriteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 21
 **/
@RestController
@Api("Favourite API Service")
public class FavouriteController {

    @Autowired
    private FavouriteService favouriteService;

    @PostMapping("/addFavourite")
    @ApiOperation("Add the matches to favourite")
    @ApiImplicitParam(name = "favourite",value = "favourite",required = true,dataTypeClass = Favourite.class)
    public ResponseEntity<Favourite> addFavourite(@RequestBody @Validated Favourite favourite) throws Exception {
        Favourite favourite1 = favouriteService.save(favourite);
        return new ResponseEntity<>(favourite1, HttpStatus.OK);
    }

    @GetMapping("/getFavourite")
    @ApiOperation("Query the favourite matches")
    @ApiImplicitParam(name = "userId",value = "userId",required = true,dataTypeClass = String.class)
    public ResponseEntity<List<Favourite>> getFavourite(@RequestParam String userId) throws Exception {
        if(StringUtils.isEmpty(userId)){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        List<Favourite> favouriteList = favouriteService.getFavourites(userId);
        return new ResponseEntity<>(favouriteList, HttpStatus.OK);
    }
}
