package com.stackroute.favouriteservice.repository;

import com.stackroute.favouriteservice.domian.Favourite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 17
 **/
@Repository
public interface FavouriteRepository extends JpaRepository<Favourite, String> {

    public Favourite findByUniqueIdAndUserId(String uniqueId, String userId);

    public List<Favourite> findByUserId(String userId);

}
