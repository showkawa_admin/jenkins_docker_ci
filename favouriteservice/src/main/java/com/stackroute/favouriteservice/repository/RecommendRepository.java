package com.stackroute.favouriteservice.repository;

import com.stackroute.favouriteservice.domian.Recommended;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 17
 **/
@Repository
public interface RecommendRepository extends JpaRepository<Recommended, String> {


}
