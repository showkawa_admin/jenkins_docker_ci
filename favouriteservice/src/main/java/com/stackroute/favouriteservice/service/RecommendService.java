package com.stackroute.favouriteservice.service;

import com.stackroute.favouriteservice.domian.Recommended;
import com.stackroute.favouriteservice.repository.RecommendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 17
 **/
@Service
public class RecommendService {

    @Autowired
    private RecommendRepository recommendRepository;


    public List<Recommended> getRecommends(){
        return   recommendRepository.findAll();
    }


}
