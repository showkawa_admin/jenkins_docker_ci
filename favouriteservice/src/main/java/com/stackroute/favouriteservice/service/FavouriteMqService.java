package com.stackroute.favouriteservice.service;

import com.stackroute.favouriteservice.domian.Favourite;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @program: cimatch
 * @author: Brian Huang
 * @create: 2019-06-30 19
 **/
@Component
public class FavouriteMqService {

    @Autowired
    RabbitTemplate rabbitTemplate;
    @Value("${spring.rabbitmq.exchange}")
    private String exchange;
    @Value("${spring.rabbitmq.routingKey}")
    private String routingKey;

    @Async
    public void addFavourite(Favourite favourite){
        addFavourite(exchange,routingKey,favourite);
    }

    private void addFavourite(String exchange, String routingKey, Favourite favourite){
        rabbitTemplate.convertAndSend(exchange,routingKey,favourite);
    }
}
